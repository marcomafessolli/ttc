﻿using UnityEngine;
using System.Collections;

public class AudioGame : MonoBehaviour {

	public AudioClip introAndQuestionOne;

	public AudioClip numberOneQuestion;
	public AudioClip numberTwoQuestion;
	public AudioClip numberThreeQuestion;
	public AudioClip numberFourQuestion;

	public AudioClip questionOneErr1;
	public AudioClip questionOneErr2;
	public AudioClip questionOneRight;


	public AudioClip questionTwoErr1;
	public AudioClip questionTwoErr2;
	public AudioClip questionTwoRight;

	public AudioClip questionThreeErr1;
	public AudioClip questionThreeErr2;
	public AudioClip questionThreeRight;

	public AudioClip questionFourErr1;
	public AudioClip questionFourErr2;
	public AudioClip questionFourRight;

	private AudioSource player;

	void Start () {
		player = this.GetComponent<AudioSource> ();
	}

	public void PlayIntro() {
		player.PlayOneShot (introAndQuestionOne);
	}

	public void PlayQuestionOne() {
		player.PlayOneShot (numberOneQuestion);
	}

	public void PlayQuestionOneErr1() {
		player.PlayOneShot (questionOneErr1);
	}

	public void PlayQuestionOneErr2() {
		player.PlayOneShot (questionOneErr2);
	}

	public void PlayQuestionTwo() {
		player.PlayOneShot (numberTwoQuestion);
	}

	public void PlayQuestionTwoErr1() {
		player.PlayOneShot (questionTwoErr1);
	}

	public void PlayQuestionTwoErr2() {
		player.PlayOneShot (questionTwoErr2);
	}

	public void PlayQuestionThree() {
		player.PlayOneShot (numberThreeQuestion);
	}

	public void PlayQuestionThreeErr1() {
		player.PlayOneShot (questionThreeErr1);
	}

	public void PlayQuestionThreeErr2() {
		player.PlayOneShot (questionThreeErr2);
	}

	public void PlayQuestionFour() {
		player.PlayOneShot (numberFourQuestion);
	}

	public void PlayQuestionFourErr1() {
		player.PlayOneShot (questionFourErr1);
	}

	public void PlayQuestionFourErr2() {
		player.PlayOneShot (questionFourErr2);
	}
}
