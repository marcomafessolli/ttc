﻿using UnityEngine;
using System.Collections;

public class Bucket : MonoBehaviour {

	ArrayList bucketElements = new ArrayList();

	public void AddElementObject(Transform transform) {
		bucketElements.Add (transform);
		transform.SetParent(this.transform);
	}

	public void ClearBucket () {
		bucketElements.Clear ();
	}

	public ArrayList GetBucketElements () {
		return bucketElements;
	}
}
