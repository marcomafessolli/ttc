﻿using UnityEngine;
using System.Collections;

public class ConfirmBucket : MonoBehaviour {

	public Bucket bucket;
	public Game game;
	public AudioGame gameAudio;

	private int lvlOneTrys = 0;
	private int lvlTwoTrys = 0;
	private int lvlThreeTrys = 0;
	private int lvlFourTrys = 0;

	ArrayList levelOneElements = new ArrayList() {
		ElementEnumeration.FERRO
	};

	ArrayList levelTwoElements = new ArrayList() {
		ElementEnumeration.OURO
	};

	ArrayList levelThreeElements = new ArrayList () {
		ElementEnumeration.OXIGENIO,
		ElementEnumeration.OXIGENIO
	};

	ArrayList levelFourElements = new ArrayList () {
		ElementEnumeration.HIDROGENIO,
		ElementEnumeration.HIDROGENIO,
		ElementEnumeration.OXIGENIO
	};

	private int level = 0;

	public void Confirm () {
		switch (level) {
		case 0: 
			gameAudio.PlayIntro ();
			level = 1;
			break;
		case 1:
			ConfirmLevelOne ();
			break;
		case 2:
			ConfirmLevelTwo ();
			break;
		case 3: 
			ConfirmLevelThree ();
			break;
		case 4:
			ConfirmLevelFour ();
			break;
		}

	}

	private bool ConfirmLevel (ArrayList levelElements) {
		int matches = 0;
		ArrayList bucketElements = bucket.GetBucketElements ();

		if (bucketElements.Count != levelElements.Count) {
			return false;
		}
	
		ArrayList levelClone = (ArrayList) levelElements.Clone ();

		foreach (Transform element in bucketElements) {
			ElementEnumeration elementType = element.transform.GetComponent<ElementObject> ().elementType;

			print (elementType);
			if (levelClone.Contains(elementType)) {
				matches++;
				levelClone.Remove (elementType);
			}
		}

		print (matches);
		if (matches == levelElements.Count) {
			return true;
		} else {
			return false;
		}
	}

	private void ConfirmLevelOne () {
		if (ConfirmLevel(levelOneElements)) {
			level = 2;
			game.currentLvl = 2;
			gameAudio.PlayQuestionTwo ();
		} else {
			lvlOneTrys++;

			if (lvlOneTrys == 1) {
				gameAudio.PlayQuestionOneErr1 ();
			} else {
				gameAudio.PlayQuestionOneErr2 ();
			}
		
			ResetElements ();
		}

		bucket.ClearBucket ();
	}

	private void ConfirmLevelTwo () {
		if (ConfirmLevel (levelTwoElements)) {
			level = 3;
			game.currentLvl = 3;
			gameAudio.PlayQuestionThree ();
		} else {
			lvlTwoTrys++;

			if (lvlTwoTrys == 1) {
				gameAudio.PlayQuestionTwoErr1 ();
			} else {
				gameAudio.PlayQuestionTwoErr2 ();
			}

			ResetElements ();
		}

		bucket.ClearBucket ();
	}

	private void ConfirmLevelThree() {
		if (ConfirmLevel (levelThreeElements)) {
			level = 4;
			game.currentLvl = 4;
			gameAudio.PlayQuestionFour ();
		} else {
			lvlThreeTrys++;

			if (lvlThreeTrys == 1) {
				gameAudio.PlayQuestionThreeErr1 ();
			} else {
				gameAudio.PlayQuestionThreeErr2 ();
			}

			ResetElements ();
		}
		bucket.ClearBucket ();
	}

	private void ConfirmLevelFour() {
		if (ConfirmLevel (levelFourElements)) {
		} else {
			lvlFourTrys++;
			if (lvlFourTrys == 1) {
				gameAudio.PlayQuestionFourErr1 ();
			} else {
				gameAudio.PlayQuestionFourErr2 ();
			}

			ResetElements ();
		}
		bucket.ClearBucket ();
	}

	public void ResetElements() {
		ArrayList elements = game.GetElements ();
		foreach (ElementObject element in elements) {
			element.transform.parent = null;
			element.isAdded = false;
			element.ResetPosition ();
		}
	}
}
