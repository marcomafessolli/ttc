﻿using UnityEngine;
using System.Collections;

public class ElementObject : MonoBehaviour {

	public Game game;

	public bool isSelected = false;
	public int visibleAtLvl;

	public bool isAdded = false;
	public ElementEnumeration elementType;

	private const float selectTimer = 2;
	private float timer = 0;

	private bool atBucketCollider = false;
	private GameObject bucket;

	Vector3 startPosition;
	Quaternion startRotation;

	void Start() {
		startPosition = transform.position;
		startRotation = transform.rotation;
		game.AddElement (this);
	}
 
	void Update() {
		if (atBucketCollider) {
			StartTimer ();
		} else {
			timer = 0;
		}
			
		Renderer[] allChildRenderers = GetComponentsInChildren<Renderer>();
		if (visibleAtLvl != game.currentLvl) {
			foreach (Renderer R in allChildRenderers) {
				R.enabled = false;
			}
		} else {
			foreach (Renderer R in allChildRenderers) {
				R.enabled = true;
			}
		}
	}

	public void Move(Transform transform) {
		this.transform.SetParent (transform);
		this.transform.GetComponent<Rigidbody> ().isKinematic = true;
		this.transform.GetComponent<Rigidbody> ().useGravity = false;
		isSelected = true;
	}

	private void DropObject () {
		if (!this.isAdded) {
			bucket.GetComponent<Bucket> ().AddElementObject (this.transform);
			this.transform.GetComponent<Rigidbody> ().useGravity = true;
			isSelected = false;

			this.isAdded = true;
		}
	}

	public void StartTimer () {
		if (timer >= selectTimer) {
			DropObject ();
		}	
		timer += Time.deltaTime;
	}

	public void ResetPosition () {
		transform.position = startPosition;
		transform.rotation = startRotation;
	}

	void OnTriggerEnter(Collider other) {
		GameObject gameObject = other.gameObject;
		if (gameObject.GetComponent<Bucket> () != null) {
			atBucketCollider = true;
			bucket = gameObject;
		}
	}


	void OnTriggerExit(Collider other) {
		atBucketCollider = false;
		bucket = null;
	}

}
