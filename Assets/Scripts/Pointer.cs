﻿using UnityEngine;
using System.Collections;

public class Pointer : MonoBehaviour {
	public Transform sceneCamera;

	private const float selectTimer = 2;
	private float timer = 0;

	private RaycastHit hit;

	void Update () {
		RayCast ();
	}

	private void RayCast() {
		Vector3 fwd = transform.TransformDirection (Vector3.forward);
		if (Physics.Raycast (transform.position, fwd, out hit, 100)) {

			if (hit.transform.GetComponent<ElementObject>() != null) {
				ElementObjectOnRayCast ();
			}	

			if (hit.transform.GetComponent<ConfirmBucket>() != null) {
				ConfirmBucketOnRayCast ();
			}	

			if (hit.transform.GetComponent<ResetBucket>() != null) {
				ResetBucketOnRayCast ();
			}	
		}
	}
		

	public void StartTimer () {
		timer += Time.deltaTime;
	}


	private void ElementObjectOnRayCast() {
		if (hit.transform.GetComponent<ElementObject> ().isSelected) {
			return;
		}

		StartTimer ();

		if (timer >= selectTimer) {
			hit.transform.GetComponent<ElementObject>().Move(transform) ;
			timer = 0;
		}	
	}

	private void ConfirmBucketOnRayCast() {
		StartTimer ();

		if (timer >= selectTimer) {
			timer = 0;
			hit.transform.GetComponent<ConfirmBucket> ().Confirm ();
		}	
	}

	private void ResetBucketOnRayCast() {
		StartTimer ();

		if (timer >= selectTimer) {
			timer = 0;
			hit.transform.GetComponent<ResetBucket> ().ResetElements ();
		}	
	}
 }
