﻿using UnityEngine;
using System.Collections;

public class ResetBucket : MonoBehaviour {

	public Game game;

	public void ResetElements() {
		ArrayList elements = game.GetElements ();
		foreach (ElementObject element in elements) {
			element.transform.parent = null;
			element.isAdded = false;
			element.ResetPosition ();
		}
	}
}
